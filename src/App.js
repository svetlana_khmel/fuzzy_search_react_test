import React, { Component } from 'react';
import list from '../www/dataJson';
import FuzzySearch from 'react-fuzzy';

class App extends Component {
  constructor () {
    super();

    this.state={list: []};
  }

  componentDidMount() {
    fetch('http://localhost:3300/data')
      .then(response => response.json())
      .then(data => {
        console.log('data ---',data);
        this.setState({ list: data })})
  }

  render() {
    if(this.state.list.length === 0) return(<data>...Loading...</data>);
    const parsedList = this.state.list;
    console.log(parsedList);
    return (
      <div>
        <FuzzySearch
          list={parsedList}
          keys={['symbolName', 'description']}
          width={430}
         // onSelect={action('selected')}
          resultsTemplate={(props, state, styles, clickHandler) => {
            return state.results.map((val, i) => {
              const style = state.selectedIndex === i ? styles.selectedResultStyle : styles.resultsStyle;
              return (
                <div
                  key={i}
                  style={style}
                  onClick={() => clickHandler(i)}
                >
                  <span> {val.category} </span>
                  <span> {val.description} </span>
                  <span style={{ float: 'left', opacity: 0.5 }}>{val.symbolName} </span>
                </div>
              );
            });
          }}
        />
      </div>
    );
  }
}

export default App;



